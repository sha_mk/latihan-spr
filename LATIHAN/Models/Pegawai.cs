﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LATIHAN.Models
{
    public class Pegawai:Pengguna
    {
        public int IC { get; set; }
        public DateTime? TarikhJawatan { get; set; }
    }
}