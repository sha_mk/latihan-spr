﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LATIHAN.Models
{
    public class Latihan
    {
        public int ID { get; set; }
        [Required]
        public string NamaLatihan { get; set; }
        [Required]
        public string Anjuran { get; set; }
        public DateTime? TarikhMula { get; set; }
        public DateTime? TarikhAkhir { get; set; }
        public int BilHari { get; set; }
    }
}