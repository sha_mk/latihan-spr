﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LATIHAN.Models
{
    public class Pengguna
    {
        public int ID { get; set; }
        public string Email { get; set; }
        public string Katalaluan { get; set; }
        public string Nama { get; set; }
        public bool AktifTak { get; set; }
        public DateTime? Wujud { get; set; }
        public DateTime? Ubah { get; set; }
    }
}